'use strict';

var app = new Vue({
    el: '#app',
    data() {
        return{
            cubifyText: 'racecar',
            vertspreadText: 'twodimensions'
        }
    },
    filters:{
        cubify: cubify,
        vertspread: vertspread
    }
  })
//TODO: add input validation
//add copy button
//mobile friendly
//styling - increase size of input


function vertspread(input){
    var word = input.toUpperCase();
    var size = input.length;
    var matrix = createMatrix(size);
    
    matrix[0] = word.split('');
    for(var i = 0; i < word.length; i++){
        matrix[i][0] = '    ' + word[i];
        matrix[0][i] += ' ';
        
    }

    return displayMatrix(matrix);
}

//credit to /u/lumbdi and spread by their bot u/LinLeyLin
//converted to JS from python script --> https://github.com/NNTin/Cubify-Reddit
function cubify(input){

    if ((input.length < 5) || (input[0] !== input.slice(-1))){
        return 'Cannot cubify';
    }  

    var word = input.toUpperCase();
    var slash = '/';
    var gap = Math.floor(word.length / 2);
    var correction = ((word.length % 2) === 0) ? 1 : 0;
    var size = (Math.floor( word.length / 2 ) + parseInt(word.length + 1));
    var matrix = createMatrix(size);


    for(var i = 0; i < word.length; i++){
        // write horizontal words
        matrix[0][gap + i] = word[i];                                           //back top 
        matrix[gap][i] = word[i];                                               //front top
        matrix[2 * gap - correction][gap + i] = word[i];                        //back bottom 
        matrix[3 * gap - correction][i] = word[i];                              //front bottom 

        //write vertical words
        matrix[gap + i][0] = word[i];                                           //front left 
        matrix[i][gap] = word[i];                                               //back left 
        matrix[i][3 * gap - correction] = word[i];                              //back right 
        matrix[gap + i][2 * gap - correction] = word[i];                        //front right 
    }

    for(var i = 1; i < gap; i++){
        //add slashes
        matrix[gap - i][i] = slash;                                             //top left 
        matrix[gap - i][2 * gap + i - correction] = slash;                      //top right 
        matrix[3 * gap - i - correction][i] = slash;                            //bottom left 
        matrix[3 * gap - i - correction][2 * gap + i - correction] = slash;     //bottom right
    }    

    //for each row in matrix, add space to each letter
    //for reddit formatting, add 4 spaces to start of each row
    for(var i = 0; i < matrix.length; i++){
        matrix[i][0] = '    ' + matrix[i][0];
        for(var j = 0; j < matrix.length; j++){
            matrix[i][j] += ' ';
        }
    }

    return displayMatrix(matrix);
}
function createMatrix(size){
    var matrix = [];
    //create matrix filled with placeholder
    for(var i = 0; i < size; i++ ){
        matrix[i] = [];
        for (var j = 0; j < size; j++) { 
            matrix[i][j] = ' ';
        }
    }
    return matrix;
}
function displayMatrix(matrix){
    var string = '';
    matrix.forEach(function(x){
        x.forEach(function(y){
            string += y;
        });
        string += '\n';
    });
    return string;
}
